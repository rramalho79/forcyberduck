<?php

return [
    //menu
    //
    //company table
    'id'=>'Id',
    //company create
    'c_name'=>'Company name',
    'c_email'=>'E-mail',
    'c_file_logo'=>'Logo',
    'c_website'=>'Website',
    
    //employee
    'e_first_name'=>'First name',
    'e_last_name'=>'Last name',
    'e_email'=>'E-mail',
    'e_phone'=>'Phone',
    'e_company'=>'Company',
    
];
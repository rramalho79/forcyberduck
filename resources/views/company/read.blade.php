@extends('layouts.master')

@section('content')
<?php
$c=$data['c'];
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Company detail
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Company</a></li>
        <li class="active">Detail</li>
    </ol>
</section>
    

    <!-- Main content -->
    <section class="content">
        
        @if ($errors->any())
        <div class="callout callout-danger">
          <h4>Validation Errors!</h4>
          <ul>
                {!! implode('', $errors->all('<li>:message</li>')) !!}
          </ul>
        </div>
        @endif
       
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Company name:{{$c->name}}
                        </h3>

                        <!--<div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                  title="Collapse">
                            <i class="fa fa-minus"></i></button>
                          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i></button>
                        </div><p class="help-block">minimum size 100x100 px</p>onsubmit="alert('submit!');return false"-->
                    </div>

                    <div class="box-body">            
                        
                        <div class="col-xs-6 col-sm-3">
                            @if(Storage::disk('public')->exists($c->logo))
                            <img class="company-detail-logo img-responsive" src="{{Storage::url($c->logo)/*route('storage.read', ['filename' => $c->logo])*/ }}">
                            @else  
                            <i class="nopicture-icon fa fa-file-picture-o"></i>
                            @endif
                        </div>
                        <div col-xs-6 col-sm-9>
                            @if($c->email)
                            <p class="text-muted">{{$c->email}}</p>
                            @endif
                            @if($c->site)
                            <p class="text-muted"><a href='{{$c->site}}'>{{$c->site}}</a></p>
                            @endif
                        </div>                        
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->                
            </div>
            <div class="col-xs-12 col-sm-8">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                           Employees
                        </h3>

                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                  title="Collapse">
                            <i class="fa fa-minus"></i></button>
                          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i></button>
                        </div><p class="help-block">
                    </div>

                    <div class="box-body">           
                        <table id="dtEmplyees" class="table table-bordered table-striped dt-responsive nowrap" style="width:100%">
                        <thead>
                            <tr>
                              <th class="all">Id</th>
                              <th class="all" >Name</th>
                              <th >Email</th>
                              <th >Phone</th>
                              <th >Created_at</th>
                              <th >Updated_at</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ajax -->
                        </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <!-- / FORM -->

                </div>
            </div>
        </div>   
                

    </section>
    <!-- /.content -->
    @push('scripts')
    <script type="text/javascript">
   //not necessary, its alreqdy at the bottom page is loaded
   $(document).ready(function() {
		$('#dtEmplyees').DataTable( {
                    responsive: true,
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        "url": "{{ route('company.employees.datatableAjax',['company_id'=>$c->id])}}",
                        method: "POST",
                       /* "data": function ( d ) {
                            d.myKey = "myValue";
                            // d.custom = $('#myInput').val();
                            // etc
                        }*/
                    },
                     columns: [
                        { data: 'id', name: 'id' },
                        { data: 'first_name', name: 'first_name'  },
                        { data: 'email', name: 'email'  },
                        { data: 'phone', name: 'phone'  },
                        { data: 'created_at', name: 'created_at'  },
                        { data: 'updated_at', name: 'updated_at'  }
                    ]
                } );

	});     
        
    
    </script>
    @endpush
@endsection


@extends('layouts.master')

@section('content')
<?php
$c=$data['c'];
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Companies
    <small>Create new</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Company</a></li>
    <li class="active">Create</li>
  </ol>
</section>
    

    <!-- Main content -->
    <section class="content">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
              @if(Session::has('alert-' . $msg))
              <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
              @endif
            @endforeach
          </div> <!-- end .flash-message -->
        @if ($errors->any())
        <div class="callout callout-danger">
          <h4>Validation Errors!</h4>
          <ul>
                {!! implode('', $errors->all('<li>:message</li>')) !!}
          </ul>
        </div>
        @endif
       
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <!-- Default box -->
                <form name="formCompany" id="formCompany" 
                            method="post" action="{{route("company.update")}}"
                            enctype="multipart/form-data" class="form-horizontal">
                    @csrf
                  <input type="hidden" name="c_id" id="c_id" value="{{$c->id}}"/>
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Edit
                        </h3>

                        <!--<div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                  title="Collapse">
                            <i class="fa fa-minus"></i></button>
                          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i></button>
                        </div><p class="help-block">minimum size 100x100 px</p>onsubmit="alert('submit!');return false"-->
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="c_name" class="col-sm-2 control-label" >{{trans('translations.c_name')}}</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="c_name" name="c_name" placeholder="" value="{{old('c_name')?old('c_name'):$c->name}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="c_email" class="col-sm-2 control-label">{{trans('translations.c_email')}}</label>

                            <div class="col-sm-10">
                                <input id="c_email" name="c_email" type="email" class="form-control" placeholder="" value="{{old('c_email')?old('c_email'):$c->email}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="logo" class="col-sm-2 control-label">{{trans('translations.c_file_logo')}}</label>
                            <div class="col-sm-10">
                                <img id="logo_preview" class="img logo" src="{{$c->logo? Storage::url($c->logo):"/img/no-image-icon.png"}}">
                                <input style="display:none" id="c_file_logo" name="c_file_logo" type="file" accept="image/png, image/jpeg, image/gif" />
                                <a href="javascript:;" id="c_file_logo_reset" class="btn btn-default">Reset</a>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="c_website" class="col-sm-2 control-label">{{trans('translations.c_website')}}</label>
                            <div class="col-sm-10">
                                <input id="c_website" name="c_website" type="text" class="form-control" placeholder="" value="{{old('c_website')}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">
                                <button id="submitFormCompany" name="submitFormCompany" type="submit" value="button" class="btn btn-default btn-block btn-flat">
                                Submit</button>
                            </div>
                        </div>                      
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->                
            </div>
            <div class="col-xs-12 col-sm-8">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                           Employees
                        </h3>

                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                  title="Collapse">
                            <i class="fa fa-minus"></i></button>
                          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i></button>
                        </div><p class="help-block">
                    </div>

                    <div class="box-body">           
                        <table id="dtEmplyees" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                              <th>Id</th>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>Created_at</th>
                              <th>Updated_at</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ajax -->
                        </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <!-- / FORM -->

                </div>
            </div>
        </div>   
                

    </section>
    <!-- /.content -->
          
          <!-- /.box -->

    </section>
    <!-- /.content -->
    @push('scripts')
    <script type="text/javascript">
        var no_pictureIMG="/img/no-image-icon.png";
        //image preview and auxiliar data-width and data-height attributes for validation
        $("#c_file_logo_reset").click(function(e){
            e.preventDefault();
            alert("reset");
            $.ajax({
                    url: "{{ route('company.delete_image_on_update')}}",
                    method:"POST",
                    data:{

                    },
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(result){
                        var jsonData=result;//JSON.parse(result);
                        var control = $("#c_file_logo").css("display","block");
                        $("#logo_preview").attr('src', no_pictureIMG);
                    }
                    //,error: function(returnvalue) {
                    //    var message="OCORREU UM ERRO";//JSON.parse(returnvalue)
                    //    console.log("ERROR="+message.responseText.message);
                    //},
                });
            
        })
        $("#c_file_logo").change(function (e){ 
        e.preventDefault();
        var _URL = window.URL || window.webkitURL;
        
        var that=$(this);
        
        var img = new Image();
        var imgwidth = 0;
        var imgheight = 0;
        img.src = _URL.createObjectURL(this.files[0]);
        img.onload = function() {
                    imgwidth = this.width;
                    imgheight = this.height;
                    alert(imgheight)
                    that.attr('data-width', imgwidth);
                    that.attr('data-height', imgheight);
                    //$("#c_width").val(imgwidth)
                    //$("#c_height").val(imgheight)
        }
        var preview_img = $("#logo_preview");      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);            
            preview_img.attr('src', e.target.result);
        }        
        reader.readAsDataURL(file);
        //$("#formCompany").validate({ ignore: "" });
        $("#c_file_logo").valid();
    });  
        //custom methods for image dimensions
   $.validator.addMethod('minImageWidth', function(value, element, minWidth) {
       if(element.files.length == 0)return true;//its not required
        var el_w=$(element).attr('data-width');
        if( parseInt(el_w)>minWidth)return true;
        else return false;
        
      }, function(minWidth, element) {
        var imageWidth = $(element).attr('data-width');
        return (imageWidth)
            ? ("Your image's width must be greater than " + minWidth + "px")
            : "Selected file is not an image.";
    });
    $.validator.addMethod('minImageHeight', function(value, element, minHeight) 
    {
        if(element.files.length == 0)return true;//its not required
        var el_h=$(element).attr('data-height');
       if( parseInt(el_h)>minHeight)return true;
        else return false;
        
      }, function(minHeight, element) {
        var imageHeight = $(element).attr('data-height');        
        return (imageHeight)
            ? ("Your image's height must be greater than " + minHeight + "px")
            : "Selected file is not an image.";
    });
    
   $(document).on("click","#submitFormCompany" , function(e){
       //e.preventDefault();
       if($("#formCompany").valid()){
           if($("#c_id").val()>0){
               $(this).attr("action","{{route("company.update")}}");
           }else{
               $(this).attr("action","{{route("company.store")}}");
           }
           alert("valid=>submit now"+$(this).serialize()) 
           //alert($(this).attr("action"));
           $(this).submit();
           
       }
   })/*
   $(document).on("submit","#formCompany",function(e){
       alert("submit event");
   });*/
   /*
   $("#formCompany").submit(function(e){
       //e.preventDefault();
       if($("#formCompany").valid()){
           if($("#c_id").val()>0){
               $("#formCompany").attr("action","{{route("company.update")}}");
           }else{
               $("#formCompany").attr("action","{{route("company.store")}}");
           }
           alert("submit now"+$("#formCompany").length)           
           //$("#formCompany").submit();
           return true;
       }else return false;//stops submit
   })*/
   //not necessary, its alreqdy at the bottom page is loaded
   $(document).ready(function() {
		$("#formCompany").validate({
			//debug: true,
			
			/*success: function(label) {
				label.text("ok!").addClass("success");
			},*/
			rules: {
				c_name: {
					required: true,					
					maxlength: 255
				},				
				c_email: {
					//required:true,
                                        email:true
				},
                                c_file_logo: {
                                        minImageWidth: "100",
                                        minImageHeight: "100",
				},
                                field: {
                                    url: true
                                }

			},highlight: function(element) {
                            //console.log("3"+JSON.stringify(element))
                                $(element).closest('.form-group').addClass('has-error');
                        },
                        unhighlight: function(element) {
                           // console.log("2"+JSON.stringify(element))
                                $(element).closest('.form-group').removeClass('has-error');
                        },
                        errorElement: 'div',
                        errorClass: 'help-block',
                        errorPlacement: function(error, element) {
                            //console.log("1"+JSON.stringify(error))
                           // console.log("1"+JSON.stringify(element))
                            //if(element.parent('.input-group').length) {
                            //    error.insertAfter(element.parent());
                            //} else {
                                error.insertAfter(element);
                            //}
                        }
		});

	});     
        
    
    </script>
    @endpush
@endsection


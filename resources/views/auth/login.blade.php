@extends('layouts.app')

@section('content')
<div class="login-box">
 <div class="login-box-body">
    <p class="login-box-msg">{{ __('Login') }}</p>
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="form-group has-feedback">
           <input name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('E-Mail Address') }}" value="{{ old('email') }}"/>
           
           @if ($errors->has('email'))
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
         </div>
        <div class="form-group has-feedback">
            <input name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('Password') }}">
            
            @if ($errors->has('password'))
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">              
                <button type="submit" class="btn btn-primary">
                    {{ __('Login') }}
                </button>
            </div>
            <!-- /.col -->
            
            @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            @endif
        </div>
    </form>
    </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
@endsection

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Rui Ramalho</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul id="main-menu" class="sidebar-menu">
            <li class="header">Main Menu</li>
            <li  class="treeview @if(\Request::is('company')){{'active'}}@endif">
                <a href="{{route("company.index")}}"><i class="fa fa-cubes"></i> Companies</a>
            </li>
            <li  class="treeview @if(\Request::is('employee')){{'active'}}@endif">
                <a href="{{route("employee.index")}}"><i class="fa fa-users"></i> Employees</a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

@extends('layouts.master')

@section('content')
<?php
$e=$data['e'];
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Employee detail
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Employee</a></li>
        <li class="active">Detail</li>
    </ol>
</section>
    

    <!-- Main content -->
    <section class="content">
        
        @if ($errors->any())
        <div class="callout callout-danger">
          <h4>Validation Errors!</h4>
          <ul>
                {!! implode('', $errors->all('<li>:message</li>')) !!}
          </ul>
        </div>
        @endif
       
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            {{$e->first_name}} {{$e->last_name}}
                                                      
                        </h3>

                        <!--<div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                  title="Collapse">
                            <i class="fa fa-minus"></i></button>
                          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i></button>
                        </div><p class="help-block">minimum size 100x100 px</p>onsubmit="alert('submit!');return false"-->
                    </div>

                    <div class="box-body">            
                        <div col-xs-12 col-sm-9>
                            <div class="row">
                                
                                <div class="col-xs-6 col-sm-6 text-left">
                                    <p class="text-muted">
                                    @if(strlen($e->company->logo)>0)                                    
                                        <img id="logo_preview" class="img logo" src="{{Storage::url($e->company->logo)}}">
                                    @endif
                                    {{$e->company->name}}<br/> 
                                    @if($e->email)
                                    <b>Email: </b>{{$e->email}}<br/>
                                    @endif
                                    @if($e->phone)
                                    <b>Phone: </b>{{$e->phone}}<br/>
                                    @endif
                                    </p>
                                </div>
                            </div>  
                                
                            </tr>
                            
                            
                        </div>                        
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->                
            </div>
        </div>   
                

    </section>
    <!-- /.content -->
    @push('scripts')
    <script type="text/javascript">
   
    
    </script>
    @endpush
@endsection


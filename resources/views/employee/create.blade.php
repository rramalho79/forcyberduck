@extends('layouts.master')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Employees
    <small>Create new</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Employee</a></li>
    <li class="active">Create</li>
  </ol>
</section>
    

    <!-- Main content -->
    <section class="content">
        
        @if ($errors->any())
        <div class="callout callout-danger">
          <h4>Validation Errors!</h4>
          <ul>
                {!! implode('', $errors->all('<li>:message</li>')) !!}
          </ul>
        </div>
        @endif
        
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
          
          </h3>

          <!--<div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div><p class="help-block">minimum size 100x100 px</p>onsubmit="alert('submit!');return false"-->
        </div>
          <form name="formEmployee" id="formEmployee" 
                method="post" action="{{route("employee.store")}}"
                enctype="multipart/form-data" class="form-horizontal">
              <div class="box-body">
                  <!-- FORM -->
                  @csrf
                  <input type="hidden" name="e_id" id="e_id"/>
                  <div class="form-group">
                      <label for="e_first_name" class="col-sm-2 control-label" >{{trans('translations.e_first_name')}}</label>
                      <div class="col-sm-10">
                          <input type="text" class="form-control" id="e_first_name" name="e_first_name" placeholder="" value="{{old('e_first_name')}}">
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="e_last_name" class="col-sm-2 control-label" >{{trans('translations.e_last_name')}}</label>
                      <div class="col-sm-10">
                          <input type="text" class="form-control" id="e_last_name" name="e_last_name" placeholder="" value="{{old('e_last_name')}}">
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="c_email" class="col-sm-2 control-label">{{trans('translations.e_email')}}</label>

                      <div class="col-sm-10">
                          <input id="e_email" name="e_email" type="email" class="form-control" placeholder="" value="{{old('e_email')}}">
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="e_phone" class="col-sm-2 control-label">{{trans('translations.e_phone')}}</label>

                      <div class="col-sm-10">
                          <input id="e_phone" name="e_phone" type="text" class="form-control" placeholder="" value="{{old('e_phone')}}">
                      </div>
                  </div>

                  <div class="form-group">
                      <label for="e_company" class="col-sm-2 control-label">{{trans('translations.e_company')}}</label>
                      <div class="col-sm-10">
                          
                            <select name="e_company" id="e_company" class="form-control select2" style="width: 100%;">
                                
                              @foreach($data["companies"] as $c)
                              <option value="{{$c->id}}">{{$c->name}}</option>
                              @endforeach
                            </select>
                          </div>
                          <!-- /.form-group -->
                    </div>
                 

                  <div class="form-group">
                      <div class="col-sm-10 col-sm-offset-2">
                          <button id="submitFormEmployee" name="submitFormEmployee" type="submit" value="button" class="btn btn-default btn-block btn-flat">
                          Submit</button>
                      </div>
                  </div>



              </div>
              <!-- /.box-body -->
          </form>
            <!-- / FORM -->
        
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    @push('scripts')
    <script type="text/javascript">
        
    
   $(document).on("click","#submitFormEmployee" , function(e){
      
      // e.preventDefault();
       if($("#formEmployee").valid()){
          // if($("#e_id").val()>0){
          //     $(this).attr("action","{{route("employee.update")}}");
          // }else{
          //     $(this).attr("action","{{route("employee.store")}}");
          // }
           //alert("valid=>submit now"+$(this).serialize()) 
           //alert($(this).attr("action"));
           
            $(this).submit();
           
       }
   })/*
   $(document).on("submit","#formCompany",function(e){
       alert("submit event");
   });*/
   /*
   $("#formCompany").submit(function(e){
       //e.preventDefault();
       if($("#formCompany").valid()){
           if($("#c_id").val()>0){
               $("#formCompany").attr("action","{{route("company.update")}}");
           }else{
               $("#formCompany").attr("action","{{route("company.store")}}");
           }
           alert("submit now"+$("#formCompany").length)           
           //$("#formCompany").submit();
           return true;
       }else return false;//stops submit
   })*/
   //not necessary, its alreqdy at the bottom page is loaded
   $(document).ready(function() {
		$("#formEmployee").validate({
			//debug: true,
			
			/*success: function(label) {
				label.text("ok!").addClass("success");
			},*/
			rules: {
				e_first_name: {
					required: true,					
					maxlength: 255
				},				
				e_last_name: {
					required: true,					
					maxlength: 255
				},				
				e_email: {
					//required:true,
                                        email:true
				},
                                e_phone: {
                                        //number:true
                                        //minImageHeight: "100",
				},
                                company: {
                                    required: true,	
                                }

			},highlight: function(element) {
                            //console.log("3"+JSON.stringify(element))
                                $(element).closest('.form-group').addClass('has-error');
                        },
                        unhighlight: function(element) {
                           // console.log("2"+JSON.stringify(element))
                                $(element).closest('.form-group').removeClass('has-error');
                        },
                        errorElement: 'div',
                        errorClass: 'help-block',
                        errorPlacement: function(error, element) {
                            //console.log("1"+JSON.stringify(error))
                           // console.log("1"+JSON.stringify(element))
                            //if(element.parent('.input-group').length) {
                            //    error.insertAfter(element.parent());
                            //} else {
                                error.insertAfter(element);
                            //}
                        }
		});

	});     
        
    //Initialize Select2 Elements
    $('.select2').select2()
    </script>
    @endpush
@endsection


@extends('layouts.master')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Employees List
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="#">Employee</a></li>
    <li class="active">List</li>
  </ol>
</section>
    

    <!-- Main content -->
    <section class="content">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
              @if(Session::has('alert-' . $msg))

              <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
              @endif
            @endforeach
          </div> <!-- end .flash-message -->
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Employees list
                
            </h3>
          <div class="box-tools pull-right">
              <a type="button" class="btn btn-default btn-flat" href="javascript:;" data-action-name="delete_selected"><i class="fa fa-trash"></i>&nbsp;Delete selected</a>
                <a type="button" class="btn btn-default btn-flat" href="{{route('employee.create')}}"><i class="fa fa-plus-circle"></i>&nbsp;Create employee</a>
                <!--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i>
                </button>
                -->
          </div>
        </div>
         <div class="box-body table-responsive no-padding">
                <table class="listtable table table-hover">
                <tr>
                  <th width="120">
                      <div class="input-group">
                        <span class="input-group-addon" style="border-right:1px solid #ccc">
                          <input id="checkAll" name="checkAll" type="checkbox" value="---">
                        </span>
                        <a class="orderBy" style="display:block;padding-left:5px;padding-top:5px;" data-field="id" href="javascript:;">Id</a>  
                      </div>
                      
                  </th>
                  <th><a class="orderBy" data-field="first_name" href="javascript:;">First Name</a></th>
                  <th><a class="orderBy"  data-field="last_name" href="javascript:;">Last Name</a></th>
                  <th><a class="orderBy"  data-field="c_name" href="javascript:;">Company</a></th>
                  <th><a class="orderBy"  data-field="email" href="javascript:;">Email</a></th>
                  <th><a class="orderBy"  data-field="phone" href="javascript:;">Phone</a></th>
                  <th><a class="orderBy"  data-field="created_at" href="javascript:;">created_at</a></th>
                  <th><a class="orderBy"  data-field="updated_at" href="javascript:;">updated_at</a></th>
                  <th class="text-right">Actions</th>
                </tr>
                 @foreach ($employees as $e)
                <tr>
                    <td><div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="checkbox_actions[]" value="" data-action-id="{{ $e->id }}">
                        </span>
                            <input type="text" class="form-control" readonly value="{{ $e->id }}">
                  </div></td>
                    <td align="left">{{ $e->first_name }}</td>
                    <td align="left">{{ $e->last_name }}</td>
                    <td>{{ $e->company->name }}</td>
                    <td>{{ $e->email }}</td>
                    <td>{{ $e->phone }}</td>
                    <td>{{ $e->created_at }}</td>
                    <td>{{ $e->updated_at }}</td>
                    <td align="right">
                       <div class="btn-group-horizontal">
                        <button type="button" data-action-name="read" data-action-id="{{ $e->id }}" class="btn btn-default btn-flat"><i class="fa fa-eye"></i></button>
                        <button type="button" data-action-name="edit"  data-action-id="{{ $e->id }}" class="btn btn-default /*btn-xs btn-primary*/ btn-flat"><i class="fa fa-edit"></i></button>
                        <button type="button" data-action-name="delete"  data-action-id="{{ $e->id }}" class="btn btn-default /*btn-xs btn-danger*/ btn-flat"><i class="fa fa-trash"></i></button>
                      </div>
                    </td>
                </tr>
                @endforeach
                </table>
          </div>
          <div class="ajax-content">
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            {{ $employees->appends(['q' => $q,'o'=>$o,'ob'=>$ob ])->links() }}
            <!--<ul class="pagination pagination-sm no-margin pull-right">
              <li><a href="#">&laquo;</a></li>
              <li><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">&raquo;</a></li>
            </ul>-->
          </div>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
@push('scripts')
    <script type="text/javascript">
//check all button---        
$(document).on("click", "input[name='checkAll']",function(){
    //alert("changed")
    $("input:checkbox[name='checkbox_actions[]']").prop('checked', $(this).prop("checked")?true:false);
})
//order button---
$(document).on("click", ".listtable .orderBy",function(){
        
         location.href="?q={{$q}}&o="+$(this).attr("data-field")+"&ob={{$ob}}";
});
//read and edit buttons---
$(document).on("click", ".listtable *[data-action-name='edit']",function(){
         location.href="/employee/edit/"+$(this).attr("data-action-id");
});
$(document).on("click", ".listtable *[data-action-name='read']",function(){
         location.href="/employee/read/"+$(this).attr("data-action-id");
});
//------------------------
var selectors=".listtable *[data-action-name='delete'],";
    selectors+="*[data-action-name='delete_selected']";
$(document).on("click", selectors,function(){
    alert("CLICKED")
       //delete one or many
       var ids=new Array();
       var target="";
       var confirm_message="";
       if($(this).attr('data-action-name')=='delete_selected'){
           
           $('input[type="checkbox"][name="checkbox_actions[]"]:checked').each(function(){
                ids.push($(this).attr("data-action-id"));
                console.log($(this).attr("data-action-id"))
            });
            
            confirm_message="Delete all "+ids.length+" selected employees?";
       }else if($(this).attr('data-action-name')=='delete'){
           
            target=$(this).attr("data-action-id");
            confirm_message="Delete employee?";
        }
       //confirm
       bootbox.confirm({
            message: confirm_message,
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-default btn-flat'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-default btn-flat'
                }
            },
            callback: function (result) {
                if(result==true){
                    
                    $.ajax({
                        url: "{{ route('employee.destroy')}}",
                        method:"POST",
                        data:{
                            "ids":ids,    
                            //"id":target
                        },
                        //dataType: 'json',
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(result){

                            if(result=="error")
                            {
                                alert("error");
                            }else{
                                //alert("NO ERROR=>"+result)
                                location.reload();
                            }
                        },
                        error: function(returnvalue) {
                                
                        //    var message="OCORREU UM ERRO";//JSON.parse(returnvalue)
                        //    console.log("ERROR="+message.responseText.message);
                        },
                    });
                }
            }
        });
        
       

});

$('input[type="checkbox"][name="checkbox_actions[]"]').prop("checked",false);
    </script>
@endpush
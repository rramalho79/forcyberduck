<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
        for($i=20;$i>=0;$i--){
            DB::table('companies')->insert([
                'name' => $i."__".Str::random(10),
                'email' => 'admin@admin.com',
                'logo' => "logo".Str::random(10),
                'site' => "www.".Str::random(10),
            ]);
        }
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

//auth protected routes
Route::middleware(['auth'])->group(function(){    
    
    Route::get('/home', 'HomeController@index')
        ->name('home');

    Route::prefix("company")
      ->group(function () {          
          Route::get('/','CompanyController@index')->name('company.index');
          Route::get('/create','CompanyController@create')->name('company.create');
          Route::get('/read/{id}','CompanyController@show')->name('company.read');
          //
          Route::post('/store','CompanyController@store')->name('company.store');
          
          
          Route::get('/edit/{id}','CompanyController@edit')->name('company.edit');
          Route::post('/update','CompanyController@update')->name('company.update');
          Route::post('/mark-delete-image','CompanyController@delete_image_on_update')->name('company.delete_image_on_update');
          
          Route::post('/delete','CompanyController@destroy')->name('company.destroy');
        //Route::get('/index','CompanyController@index')->name('company.index');
          
          Route::post('/datatables','CompanyController@datatableEmployeesAjax')->name('company.employees.datatableAjax');
      });
    Route::prefix("employee")
      ->group(function () {
          
          Route::get('/','EmployeeController@index')->name('employee.index');
          Route::post('/delete','EmployeeController@destroy')->name('employee.destroy');
          Route::get('/create','EmployeeController@create')->name('employee.create');
          Route::get('/read/{id}','employeeController@show')->name('company.read');
          //
          Route::post('/store','EmployeeController@store')->name('employee.store');
          
          Route::get('/edit/{id}','EmployeeController@edit')->name('employee.edit');
            Route::post('/update','EmployeeController@update')->name('employee.update');
          //Route::get('/index','CompanyController@index')->name('company.index');
      });  
    
});
// / auth protected routes

Route::get('storage/{filename}', 'StorageController@read')->name('storage.read');


<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Company;
class Employee extends Model
{
    //
    protected $table="employees";
    
    public function company()
    {
        $instance = $this;
        $instance = $this->belongsTo(Company::class,'company_id','id');
        return $instance;
        
    } 
}

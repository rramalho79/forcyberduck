<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Employee;
class Company extends Model
{
    //
    protected $table="companies";
     
    public function employees()
    {
        $instance = $this;
        $instance = $this->hasMany(Employee::class);
        return $instance;
        
    } 
    
}

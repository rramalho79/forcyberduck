<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use App\Company;
use App\Employee;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use Datatables;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   /* public function datatableAjax(Request $request){
        
        return datatables()
                ->of(Employee::query())
                 ->addColumn('actions', function ($row) {
                    return '<a href="javascript:;" class="btn btn-xs btn-primary" data-context="datatable-action-button" data-action-name="edit" data-id="'.$row->id.'"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                })
                ->rawColumns(['actions'])
            ->make(true);
            //->toJson();
    }
    * 
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //pagination
         $employees = Employee::with("company")
                        ->where('id','>',0);
         
         if(strlen(Input::get("q"))>0)
         {
             $q= str_replace(" ","%", trim(Input::get("q")));
             $employees =  $employees->where("first_name","like","%$q%")
                                        ->orWhere("last_name","like","%$q%")
                                        ->orWhere("email","like","%$q%")
                                        ->orWhere("phone","like","%$q%") ;
         }
         
         $ob="";
         if(Input::get("ob"))
         {    
             if(Input::get("ob")=="asc")
             {
                 $ob="desc";
             }else{
                 $ob="asc";
             }
         }else{
             $ob="asc";
         }
         
        if(strlen(Input::get("o"))>0){
             switch(Input::get("o")){
                 case "id" :   $employees =  $employees->orderBy("id",$ob);
                                break; 
                 case "first_name" :   $employees =  $employees->orderBy("first_name",$ob);
                                break;
                 case "last_name" :   $employees =  $employees->orderBy("last_name",$ob);
                                break;
                 case "email":   $employees =  $employees->orderBy("email",$ob);
                                break; 
                 case "phone":   $employees =  $employees->orderBy("phone",$ob);
                                break; 
                 case "created_at":   $employees =  $employees->orderBy("created_at",$ob);
                                break;
                 case "updated_at":   $employees =  $employees->orderBy("updated_at",$ob);
                                break;           
             }
             
         }
         $employees =  $employees->paginate(10);
         
        return view('employee.index', ['employees' => $employees, "q"=>Input::get("q"),"o"=>Input::get("o"),"ob"=>$ob]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //just get the view width the companies
        $companies=Company::where("id",'>',1)->get();
        $data=array();
        $data["companies"]=$companies;
        return view('employee.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules=[
            'e_first_name'=>'required',
            'e_last_name'=>'required',
            'e_email' => 'nullable|email',//|unique:companies,email',
            //'c_phone' => 'nullable',//|unique:companies,email',
            'c_company'=> 'exists:companies,id',
            ];
        $messages=array(
            //name
            "e_first_name.required"=>trans("validation.invalid",['attribute'=>trans('translations.e_first_name')]),
            "e_last_name.required"=>trans("validation.invalid",['attribute'=>trans('translations.e_last_name')]),
            //email
            //"c_email.required"=>trans("validation.invalid",['attribute'=>trans('translations.c_email')]),
            "e_email.email"=> trans("validation.email",['attribute'=>trans('translations.email')]),
            
            
            
            //defaults
        );
        $validator = Validator::make($request->all(), $rules, $messages); 
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(($request->all()));
        }else{
            
            //echo "store";
            $e=new Employee();
            $e->first_name=$request->input("e_first_name");
            $e->last_name=$request->input("e_last_name");
            $e->email=strtolower(trim($request->input("e_email")));
            $e->phone=strtolower(trim($request->input("e_phone")));
            $e->company_id=$request->input("e_company");
            $e->save();
            return Redirect::to(route("employee.index"))->with('alert-success', 'Employee was successful created!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rules=[
            'id'=>'required|exists:employees,id',
            
            ];
        $messages=array(
            //name
            "id.required"=>trans("validation.invalid",['attribute'=>trans('translations.employee')])   
            
            //defaults
        );
        $validator = Validator::make(array('id'=>$id), $rules, $messages);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }else{
            $data=array();
            $data['e']=Employee::find($id);
            return view('employee.read',compact('data'));
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rules=[
            'id'=>'required|exists:employees,id',
            
            ];
        $messages=array(
            //name
            "id.required"=>trans("validation.invalid",['attribute'=>trans('translations.employee')])   
            
            //defaults
        );
        $validator = Validator::make(array('id'=>$id), $rules, $messages);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }else{
            $data=array();
            $data['e']=Employee::find($id);
            $companies=Company::where("id",'>',1)->get();
            $data["companies"]=$companies;
            return view('employee.edit',compact('data'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $rules=[
            'e_id'=>'required|exists:companies,id',
            'e_first_name'=>'required',
            'e_last_name'=>'required',
            'e_email' => 'nullable|email',//|unique:companies,email',
            //'c_phone' => 'nullable',//|unique:companies,email',
            'c_company'=> 'exists:companies,id',
            ];
        $messages=array(
            //name
            "e_first_name.required"=>trans("validation.invalid",['attribute'=>trans('translations.e_first_name')]),
            "e_last_name.required"=>trans("validation.invalid",['attribute'=>trans('translations.e_last_name')]),
            //email
            //"c_email.required"=>trans("validation.invalid",['attribute'=>trans('translations.c_email')]),
            "e_email.email"=> trans("validation.email",['attribute'=>trans('translations.email')]),
            
            
            
            //defaults
        );
        $validator = Validator::make($request->all(), $rules, $messages); 
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(($request->all()));
        }else{
            
            //echo "store";
            $e=Employee::find($request->input("e_id"));
            $e->first_name=$request->input("e_first_name");
            $e->last_name=$request->input("e_last_name");
            $e->email=strtolower(trim($request->input("e_email")));
            $e->phone=strtolower(trim($request->input("e_phone")));
            $e->company_id=$request->input("e_company");
            $e->save();
            return Redirect::to(route("employee.index"))->with('alert-success', 'Employee was successful updated!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

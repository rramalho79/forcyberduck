<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use App\Company;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

use App\Employee;
use Yajra\DataTables;

use Illuminate\Support\Facades\Session;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //pagination
         $companies = Company::where('id','>',0);
         
         if(strlen(Input::get("q"))>0)
         {
             $q= str_replace(" ","%", trim(Input::get("q")));
             $companies =  $companies->where("name","like","%$q%")
                                        ->orWhere("email","like","%$q%");
         }
         
         $ob="";
         if(Input::get("ob"))
         {    
             if(Input::get("ob")=="asc")
             {
                 $ob="desc";
             }else{
                 $ob="asc";
             }
         }else{
             $ob="asc";
         }
         
        if(strlen(Input::get("o"))>0){
             switch(Input::get("o")){
                 case "id" :   $companies =  $companies->orderBy("id",$ob);
                                break; 
                 case "name" :   $companies =  $companies->orderBy("name",$ob);
                                break;
                 case "email":   $companies =  $companies->orderBy("email",$ob);
                                break; 
                 case "site":   $companies =  $companies->orderBy("site",$ob);
                                break; 
                 case "created_at":   $companies =  $companies->orderBy("created_at",$ob);
                                break;
                 case "updated_at":   $companies =  $companies->orderBy("updated_at",$ob);
                                break;           
             }
             
         }
         $companies =  $companies->paginate(10);
         
        return view('company.index', ['companies' => $companies, "q"=>Input::get("q"),"o"=>Input::get("o"),"ob"=>$ob]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //just get the view
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $rules=[
            'c_name'=>'required',
            'c_email' => 'nullable|email',//|unique:companies,email',
            'c_file_logo'=> 'mimes:jpeg,gif,png|dimensions:min_width=100,min_height=100',
            'c_website'=>'url'
            ];
        $messages=array(
            //name
            "c_name.required"=>trans("validation.invalid",['attribute'=>trans('translations.c_name')]),
            //email
            //"c_email.required"=>trans("validation.invalid",['attribute'=>trans('translations.c_email')]),
            "c_email.email"=> trans("validation.email",['attribute'=>trans('translations.email')]),
            "c_email.unique"=> trans("validation.unique",['attribute'=>trans('translations.email')]),
            //logo
            "c_file_logo.mimes"=>trans("validation.mimes",['attribute'=>trans('translations.c_file_logo'),'values'=>'jpeg,gif,png']),
            "c_file_logo.dimensions"=>trans("validation.dimensions",['attribute'=>trans('translations.c_file_logo')]),
            
            
            //defaults
        );
        $validator = Validator::make($request->all(), $rules, $messages); 
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(($request->all()));
        }else{
            
            //echo "store";
            $c=new Company();
            $c->name=$request->input("c_name");
            $c->email=strtolower(trim($request->input("c_email")));
            $c->site=$request->input("c_website");
            
            $file = $request->file('c_file_logo');
            if($file){
                $extension=$file->getClientOriginalExtension();
                $newname = sha1(date('YmdHis') . str_random(30)).".$extension";
                
                Storage::disk('public')->put($newname, file_get_contents($file), 'public');
                unset($file);
                $c->logo=$newname;
            }
            
            $c->save();
            return Redirect::to(route("company.index"))->with('alert-success', 'Company was successful added!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Session::forget("company.delete_image_on_update");
        Session::forget("company");
        $rules=[
            'id'=>'required|exists:companies,id',
            
            ];
        $messages=array(
            //name
            "id.required"=>trans("validation.invalid",['attribute'=>trans('translations.company')])   
            
            //defaults
        );
        $validator = Validator::make(array('id'=>$id), $rules, $messages);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }else{
            $data=array();
            $data['c']=Company::find($id);
            return view('company.read',compact('data'));
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Session::forget("company.delete_image_on_update");
        Session::forget("company");
        $rules=[
            'id'=>'required|exists:companies,id',
            
            ];
        $messages=array(
            //name
            "id.required"=>trans("validation.invalid",['attribute'=>trans('translations.company')])   
            
            //defaults
        );
        $validator = Validator::make(array('id'=>$id), $rules, $messages);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }else{
            $data=array();
            $data['c']=Company::find($id);
            return view('company.edit',compact('data'));
        }
    }
    public function delete_image_on_update(Request $request){
        
        Session::put('company.delete_image_on_update',true);
        return "true";//indiferent
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules=[
            'c_id'=>'required|exists:companies,id',
            'c_name'=>'required',
            'c_email' => 'nullable|email',//|unique:companies,email',
            'c_file_logo'=> 'mimes:jpeg,gif,png|dimensions:min_width=100,min_height=100',
            'c_website'=>'nullable|url'
            ];
        $messages=array(
            //name
            "c_name.required"=>trans("validation.invalid",['attribute'=>trans('translations.c_name')]),
            //email
            //"c_email.required"=>trans("validation.invalid",['attribute'=>trans('translations.c_email')]),
            "c_email.email"=> trans("validation.email",['attribute'=>trans('translations.email')]),
            "c_email.unique"=> trans("validation.unique",['attribute'=>trans('translations.email')]),
            //logo
            "c_file_logo.mimes"=>trans("validation.mimes",['attribute'=>trans('translations.c_file_logo'),'values'=>'jpeg,gif,png']),
            "c_file_logo.dimensions"=>trans("validation.dimensions",['attribute'=>trans('translations.c_file_logo')]),
            
            
            //defaults
        );
        $validator = Validator::make($request->all(), $rules, $messages); 
        if ($validator->fails()) {
           //echo "ERR=OR";
             return Redirect::back()->withErrors($validator)->withInput(($request->all()));
        }else{
            
            //echo "store";
            $c=Company::find($request->input("c_id"));
            $c->name=$request->input("c_name");
            $c->email=strtolower(trim($request->input("c_email")));
            $c->site=$request->input("c_website");
            
            if(Session::has("company.delete_image_on_update")){
               if( strlen($c->logo)>3){
                    Storage::disk('public')->delete($c->logo);
                }
                Session::forget("company.delete_image_on_update");
                Session::forget("company");
                
            }
            
            $file = $request->file('c_file_logo');
            if($file){
                
                if( strlen($c->logo)>3){
                    Storage::disk('public')->delete($c->logo);
                }
                $extension=$file->getClientOriginalExtension();
                $newname = sha1(date('YmdHis') . str_random(30)).".$extension";
                
                Storage::disk('public')->put($newname, file_get_contents($file), 'public');
                unset($file);
                $c->logo=$newname;
            }
            
            $c->save();
            $data=array();
            $data['c']=$c;
            return view('company.edit',compact('data'))->with('alert-success', 'Company was successful updated!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //ajax post
    public function destroy(Request $request)
    {
        //echo var_dump(Input::get("ids"));exit;
         $rules=[
             'id'=>[
                        'nullable',
                        'exists:companies,id',
                         function ($attribute, $value, $fail)use($request) {
                            if (is_array($request->input('ids')) && count($request->input('ids'))==0) {
                                if(strlen($request->input('id'))==0)
                                    $fail($attribute.' is invalid.');
                            }
                        }
                    ],
            'ids.*'=>[ 
                        'nullable',
                        'exists:companies,id',
                         function ($attribute, $value, $fail)use($request) {
                            if(strlen($request->input('id'))==0) {
                                if (is_array($request->input('ids')) &&  count($request->input('ids'))==0)
                                    $fail($attribute.' is invalid.');
                            }
                        }
                    ],
            ];
        
        $messages=array(
            //name
            "id.required"=>trans("validation.invalid",['attribute'=>trans('translations.company')])   
            
            //defaults
        );
        $validator = Validator::make($request->all(), $rules, $messages); 
        if ($validator->fails()) {
           //echo "ERR=OR";
             return "error";
        };
        $companiesWithEmployees=array();    
        if(is_array($request->input('ids')) && count($request->input('ids'))>0){
            
            foreach($request->input('ids') as $id)
            {
                //echo "delete($id)";
                
                $c=Company::find($id);
                if($c->employees()->count()>0){
                    
                    $employees=$c->employees()->get();
                    foreach( $employees as $employee){
                        $employee->delete();
                    }
                    $c->delete();
                }else{    
                    if( strlen($c->logo)>3){
                        Storage::disk('public')->delete($c->logo);
                    }
                               
                    $c->delete();
                }
            }
            return "Companies deleted!";
        }
        if($request->input('id')>0){
                $c=Company::find($request->input('id'));
                if( strlen($c->logo)>3){
                    Storage::disk('public')->delete($c->logo);
                }
                $c->delete();
            
                return "Companie deleted!";
        }  
        
        return "Nothing as been donne";
            
           
           
       
    }
    
    
    
    public function datatableEmployeesAjax(Request $request){
        
        //echo Input::get('company_id');exit;
        $emp=Employee::where('company_id',Input::get('company_id'))
                ->select('id',
                        'first_name',
                         'last_name',
                         'email',
                         'phone',
                         'created_at',
                         'updated_at'
                        );
        return datatables()
                ->of($emp)
                 ->editColumn('first_name', function ($row) {
                    return $row->first_name." ".$row->last_name;   
                 })
                 ->removeColumn('last_name')
            ->make(true);
    }
}

const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');


mix.copy('resources/img','public/img');


mix.combine([
    'resources/assets/datatables.net-bs/css/dataTables.bootstrap.min.css',
    'resources/assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
    'resources/assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
    'resources/assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
    'resources/assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
    
    'node_modules/admin-lte/dist/css/skins/_all-skins.min.css',
    'node_modules/admin-lte/bower_components/select2/dist/css/select2.css'
],
'public/css/styles.css');

mix.combine([
    'resources/assets/datatables.net/js/jquery.dataTables.min.js',
    'resources/assets/datatables.net-bs/js/dataTables.bootstrap.min.js',
    'resources/assets/datatables.net-buttons/js/dataTables.buttons.min.js',
    'resources/assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
    'resources/assets/datatables.net-buttons/js/buttons.flash.min.js',
    'resources/assets/datatables.net-buttons/js/buttons.html5.min.js',
    'resources/assets/datatables.net-buttons/js/buttons.print.min.js',
    'resources/assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
    'resources/assets/datatables.net-keytable/js/dataTables.keyTable.min.js',
    'resources/assets/datatables.net-responsive/js/dataTables.responsive.min.js',
    'resources/assets/datatables.net-responsive-bs/js/responsive.bootstrap.js',
    'resources/assets/datatables.net-scroller/js/dataTables.scroller.min.js',
    
    
    'node_modules/admin-lte/bower_components/select2/dist/js/select2.js',
    'resources/assets/bootbox/dist/bootbox.min.js',
    'resources/assets/bootbox/dist/bootbox.locales.min.js',
],
'public/js/scripts.js');